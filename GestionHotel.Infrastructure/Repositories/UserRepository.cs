using GestionHotel.Application.Interfaces.Repositories;
using GestionHotel.Domain.Entities;
using GestionHotel.Domain.Enums;
using GestionHotel.Domain.Interfaces.Entities;

namespace GestionHotel.Infrastructure.Repositories;

public class UserRepository : IUserRepository
{
    public IUserEntity GetUserById(Guid userId)
    {
        var databaseUser = new UserEntity()
        {
            Id = Guid.NewGuid(),
            Role = UserRole.Client,
            FirstName = "Baptiste",
            LastName = "Schell"
        };

        return databaseUser;
    }
}