using GestionHotel.Application.Interfaces.Repositories;
using GestionHotel.Domain.Entities;
using GestionHotel.Domain.Enums;
using GestionHotel.Domain.Interfaces.Entities;

namespace GestionHotel.Infrastructure.Repositories;

public class RoomsRepository : IRoomsRepository
{
    public IEnumerable<IRoomEntity> GetAvailableRooms(DateTime startDate, DateTime endDate)
    {
        //Simulation d'un retour d'une base de données
        var databaseList = new List<IRoomEntity>();
        databaseList.Add(new RoomEntity()
        {
            Capacity = 3,
            Id = Guid.NewGuid(),
            State = RoomState.New,
            Type = RoomType.Double
        });
        databaseList.Add(new RoomEntity()
        {
            Capacity = 1,
            Id = Guid.NewGuid(),
            State = RoomState.Redone,
            Type = RoomType.Suite
        });
        databaseList.Add(new RoomEntity()
        {
            Capacity = 2,
            Id = Guid.NewGuid(),
            State = RoomState.NothingToReport,
            Type = RoomType.Simple
        });

        return databaseList;
    }
}