﻿using GestionHotel.Domain.Enums;
using GestionHotel.Infrastructure.Interfaces.Dtos.Payments;

namespace GestionHotel.Infrastructure.Dtos.Payments;

public class PaymentOut : IPaymentOut
{
    public Guid Id { get; set; }
    public int Value { get; set; }
    public PaymentState State { get; set; }
}