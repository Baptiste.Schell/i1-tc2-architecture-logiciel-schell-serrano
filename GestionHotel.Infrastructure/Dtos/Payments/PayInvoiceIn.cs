﻿using GestionHotel.Infrastructure.Interfaces.Dtos.Payments;

namespace GestionHotel.Infrastructure.Dtos.Payments;

public class PayInvoiceIn : IPayInvoiceIn
{
    public Guid PaymentId { get; set; }
    public string CardNumber { get; set; } = string.Empty;
    public string ExpiryDate { get; set; } = string.Empty;
}