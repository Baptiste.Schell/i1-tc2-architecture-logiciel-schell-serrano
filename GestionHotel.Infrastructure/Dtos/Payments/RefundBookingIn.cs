﻿using GestionHotel.Infrastructure.Interfaces.Dtos.Payments;

namespace GestionHotel.Infrastructure.Dtos.Payments;

public class RefundBookingIn : IRefundBookingIn
{
    public Guid BookingId { get; set; }
    public bool ClientIsRefund { get; set; }
    public string CardNumber { get; set; } = string.Empty;
    public string ExpiryDate { get; set; } = string.Empty;
}