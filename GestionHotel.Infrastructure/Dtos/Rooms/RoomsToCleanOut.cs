﻿using GestionHotel.Infrastructure.Interfaces.Dtos.Rooms;

namespace GestionHotel.Infrastructure.Dtos.Rooms;

public class RoomsToCleanOut : IRoomsToCleanOut
{
    public IEnumerable<IRoomOut> RoomsToClean { get; set; } = new List<IRoomOut>();
    public IEnumerable<IRoomOut> RoomsWithBigDamage { get; set; } = new List<IRoomOut>();
}