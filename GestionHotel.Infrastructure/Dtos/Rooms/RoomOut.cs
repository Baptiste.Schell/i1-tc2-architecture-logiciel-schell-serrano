﻿using GestionHotel.Domain.Enums;
using GestionHotel.Infrastructure.Interfaces.Dtos.Rooms;

namespace GestionHotel.Infrastructure.Dtos.Rooms;

public class RoomOut : IRoomOut
{
    public Guid RoomId { get; set; }
    public int Capacity { get; set; }
    public RoomType Type { get; set; }     
    public RoomState State { get; set; }
}