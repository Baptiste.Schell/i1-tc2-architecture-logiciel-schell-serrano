﻿using GestionHotel.Infrastructure.Interfaces.Dtos.Others;

namespace GestionHotel.Infrastructure.Dtos.Others;

public class PeriodeIn : IPeriodIn
{
    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }
}