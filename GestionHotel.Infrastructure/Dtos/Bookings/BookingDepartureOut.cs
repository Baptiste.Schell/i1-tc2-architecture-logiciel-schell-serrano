﻿using GestionHotel.Infrastructure.Interfaces.Dtos.Bookings;
using GestionHotel.Infrastructure.Interfaces.Dtos.Payments;

namespace GestionHotel.Infrastructure.Dtos.Bookings;

public class BookingDepartureOut : IBookingDepartureOut
{
    public Guid BookingId { get; set; }
    public IEnumerable<IPaymentOut> RestOfPaiement { get; set; } = new List<IPaymentOut>();
}