﻿using GestionHotel.Infrastructure.Interfaces.Dtos.Payments;

namespace GestionHotel.Infrastructure.Interfaces.Dtos.Bookings;

public interface IBookingDepartureOut
{
    public Guid BookingId { get; set; }
    public IEnumerable<IPaymentOut> RestOfPaiement { get; set; }
}