﻿namespace GestionHotel.Infrastructure.Interfaces.Dtos.Rooms;

public interface IRoomsToCleanOut
{
    public IEnumerable<IRoomOut> RoomsToClean { get; set; }
    public IEnumerable<IRoomOut> RoomsWithBigDamage { get; set; }
}