﻿using GestionHotel.Domain.Enums;

namespace GestionHotel.Infrastructure.Interfaces.Dtos.Rooms;

public interface IRoomOut
{
    public Guid RoomId { get; set; }
    public int Capacity { get; set; }
    public RoomType Type { get; set; } 
    public RoomState State { get; set; }
}