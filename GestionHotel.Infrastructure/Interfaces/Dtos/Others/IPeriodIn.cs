﻿namespace GestionHotel.Infrastructure.Interfaces.Dtos.Others;

public interface IPeriodIn
{
    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }
}