﻿namespace GestionHotel.Infrastructure.Interfaces.Dtos.Payments;

public interface IRefundBookingIn
{
    public Guid BookingId { get; set; }
    public bool ClientIsRefund { get; set; }
    public string CardNumber { get; set; }
    public string ExpiryDate { get; set; }
}