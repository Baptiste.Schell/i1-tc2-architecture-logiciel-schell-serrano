﻿namespace GestionHotel.Infrastructure.Interfaces.Dtos.Payments;

public interface IPayInvoiceIn
{
    public Guid PaymentId { get; set; }
    public string CardNumber { get; set; }
    public string ExpiryDate { get; set; }
}