﻿using GestionHotel.Domain.Enums;

namespace GestionHotel.Infrastructure.Interfaces.Dtos.Payments;

public interface IPaymentOut
{
    public Guid Id { get; set; }
    public int Value { get; set; }
    public PaymentState State { get; set; }
}