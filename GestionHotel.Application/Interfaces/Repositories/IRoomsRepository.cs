using GestionHotel.Domain.Interfaces.Entities;

namespace GestionHotel.Application.Interfaces.Repositories;

public interface IRoomsRepository
{
    public IEnumerable<IRoomEntity> GetAvailableRooms(DateTime startDate, DateTime endDate);
}