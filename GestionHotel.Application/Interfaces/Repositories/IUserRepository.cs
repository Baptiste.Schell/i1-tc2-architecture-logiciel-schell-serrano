using GestionHotel.Domain.Interfaces.Entities;

namespace GestionHotel.Application.Interfaces.Repositories;

public interface IUserRepository
{
    public IUserEntity GetUserById(Guid userId);
}