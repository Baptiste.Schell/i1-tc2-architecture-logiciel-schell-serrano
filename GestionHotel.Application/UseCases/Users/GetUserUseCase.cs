using GestionHotel.Domain.Entities;
using GestionHotel.Domain.Interfaces.UseCases.Users;
using Microsoft.Extensions.Logging;

namespace GestionHotel.Application.UseCases.Users;

public class GetUserUseCase : GenericUseCase<GetUserUseCase>, IGetUserUseCase
{
    public GetUserUseCase(ILogger<GenericUseCase<GetUserUseCase>> logger) : base(logger)
    {
    }

    public UserEntity Handle(Guid userId)
    {
        throw new NotImplementedException();
    }
}