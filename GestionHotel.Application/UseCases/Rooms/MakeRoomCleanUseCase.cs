using GestionHotel.Domain.Interfaces.UseCases.Rooms;
using Microsoft.Extensions.Logging;

namespace GestionHotel.Application.UseCases.Rooms;

public class MakeRoomCleanUseCase : GenericUseCase<MakeRoomCleanUseCase>, IMakeRoomCleanUseCase
{
    public MakeRoomCleanUseCase(ILogger<GenericUseCase<MakeRoomCleanUseCase>> logger) : base(logger)
    {
    }
}