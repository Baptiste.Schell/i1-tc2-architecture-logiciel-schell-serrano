using GestionHotel.Domain.Interfaces.UseCases.Rooms;
using Microsoft.Extensions.Logging;

namespace GestionHotel.Application.UseCases.Rooms;

public class GetRoomsToCleanUseCase : GenericUseCase<GetRoomsToCleanUseCase>, IGetRoomsToCleanUseCase
{
    public GetRoomsToCleanUseCase(ILogger<GenericUseCase<GetRoomsToCleanUseCase>> logger) : base(logger)
    {
    }
}