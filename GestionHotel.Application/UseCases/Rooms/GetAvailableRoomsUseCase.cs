using GestionHotel.Application.Interfaces.Repositories;
using GestionHotel.Domain.Interfaces.Entities;
using GestionHotel.Domain.Interfaces.UseCases.Rooms;
using Microsoft.Extensions.Logging;

namespace GestionHotel.Application.UseCases.Rooms;

public class GetAvailableRoomsUseCase : GenericUseCase<GetAvailableRoomsUseCase>, IGetAvailableRoomsUseCase
{
    private readonly IRoomsRepository _repository;
    
    public GetAvailableRoomsUseCase(ILogger<GenericUseCase<GetAvailableRoomsUseCase>> logger, IRoomsRepository repository) : base(logger)
    {
        _repository = repository;
    }

    public IEnumerable<IRoomEntity> Handle(DateTime startDate, DateTime endDate)
    {
        try
        {
            IEnumerable<IRoomEntity> availableRoomList = _repository.GetAvailableRooms(startDate, endDate);

            return availableRoomList;
        }
        catch (Exception exception)
        {
            Logger.LogError("[GetAvailableRoomsUseCase] Handle failed with : {Exception}", exception.Message);
            throw;
        }
    }
}