using GestionHotel.Domain.Interfaces.UseCases.Payments;
using Microsoft.Extensions.Logging;

namespace GestionHotel.Application.UseCases.Payments;

public class PayInvoiceUseCase : GenericUseCase<PayInvoiceUseCase>, IPayInvoiceUseCase
{
    public PayInvoiceUseCase(ILogger<GenericUseCase<PayInvoiceUseCase>> logger) : base(logger)
    {
    }
}