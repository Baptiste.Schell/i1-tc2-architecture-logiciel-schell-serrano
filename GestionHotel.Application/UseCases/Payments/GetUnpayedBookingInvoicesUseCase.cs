using GestionHotel.Domain.Interfaces.UseCases.Payments;
using Microsoft.Extensions.Logging;

namespace GestionHotel.Application.UseCases.Payments;

public class GetUnpayedBookingInvoicesUseCase : GenericUseCase<GetUnpayedBookingInvoicesUseCase>, IGetUnpayedBookingInvoicesUseCase
{
    public GetUnpayedBookingInvoicesUseCase(ILogger<GenericUseCase<GetUnpayedBookingInvoicesUseCase>> logger) : base(logger)
    {
    }
}