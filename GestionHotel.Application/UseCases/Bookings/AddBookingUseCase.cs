using GestionHotel.Domain.Interfaces.UseCases.Bookings;
using Microsoft.Extensions.Logging;

namespace GestionHotel.Application.UseCases.Bookings;

public class AddBookingUseCase : GenericUseCase<AddBookingUseCase>, IAddBookingUseCase
{
    public AddBookingUseCase(ILogger<GenericUseCase<AddBookingUseCase>> logger) : base(logger)
    {
    }
}