using GestionHotel.Domain.Interfaces.UseCases.Bookings;
using Microsoft.Extensions.Logging;

namespace GestionHotel.Application.UseCases.Bookings;

public class DeleteBookingUseCase : GenericUseCase<DeleteBookingUseCase>, IDeleteBookingUseCase
{
    public DeleteBookingUseCase(ILogger<GenericUseCase<DeleteBookingUseCase>> logger) : base(logger)
    {
    }
}