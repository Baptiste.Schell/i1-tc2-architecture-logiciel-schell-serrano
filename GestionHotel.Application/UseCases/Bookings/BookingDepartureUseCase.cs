using GestionHotel.Domain.Interfaces.UseCases.Bookings;
using Microsoft.Extensions.Logging;

namespace GestionHotel.Application.UseCases.Bookings;

public class BookingDepartureUseCase : GenericUseCase<BookingDepartureUseCase>, IBookingDepartureUseCase
{
    public BookingDepartureUseCase(ILogger<GenericUseCase<BookingDepartureUseCase>> logger) : base(logger)
    {
    }
}