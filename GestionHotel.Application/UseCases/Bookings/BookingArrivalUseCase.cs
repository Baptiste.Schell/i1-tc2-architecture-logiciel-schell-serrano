using GestionHotel.Domain.Interfaces.UseCases.Bookings;
using Microsoft.Extensions.Logging;

namespace GestionHotel.Application.UseCases.Bookings;

public class BookingArrivalUseCase : GenericUseCase<BookingArrivalUseCase>, IBookingArrivalUseCase
{
    public BookingArrivalUseCase(ILogger<GenericUseCase<BookingArrivalUseCase>> logger) : base(logger)
    {
    }
}