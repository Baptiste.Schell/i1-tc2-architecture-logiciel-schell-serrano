using Microsoft.Extensions.Logging;

namespace GestionHotel.Application.UseCases;

public class GenericUseCase<T> where T : class
{
    protected readonly ILogger<GenericUseCase<T>> Logger;

    public GenericUseCase(ILogger<GenericUseCase<T>> logger)
    {
        Logger = logger;
    }
}