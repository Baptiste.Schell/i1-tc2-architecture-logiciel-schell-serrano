﻿using GestionHotel.Infrastructure.Dtos.Payments;
using GestionHotel.Infrastructure.Interfaces.Dtos.Payments;
using Microsoft.AspNetCore.Mvc;

namespace GestionHotel.Apis.Controllers;

[ApiController]
[ApiExplorerSettings(GroupName = "Paiements")]
[Route("paiements")]
public class PaymentsController : GenericController<PaymentsController>
{
    protected PaymentsController(ILogger<GenericController<PaymentsController>> logger) : base(logger)
    {
    }
    
    [HttpGet]
    [Route("get-unpayed-booking-invoices/{userId:guid}/{bookingId:guid}")]
    public ActionResult<List<PaymentOut>> GetUnpayedBookingInvoices([FromRoute] Guid userId, [FromRoute] Guid bookingId)
    {
        try
        {
            // var outDto = 
            return StatusCode(StatusCodes.Status200OK, "nothing now");
        }
        catch (Exception exception)
        {
            Logger.LogError("[RoomsController] GetAvailableRooms failed with the exception : {Exception}", exception.Message);
            throw;
        }
    }
    
    [HttpPut]
    [Route("pay-the-invoice/{userId:guid}")]
    public ActionResult<bool> PayTheInvoice([FromRoute] Guid userId, [FromBody] IPayInvoiceIn paymentInfos)
    {
        try
        {
            // var outDto = 
            return StatusCode(StatusCodes.Status200OK, "nothing now");
        }
        catch (Exception exception)
        {
            Logger.LogError("[RoomsController] GetAvailableRooms failed with the exception : {Exception}", exception.Message);
            throw;
        }
    }
}