﻿using GestionHotel.Infrastructure.Dtos.Bookings;
using GestionHotel.Infrastructure.Dtos.Others;
using GestionHotel.Infrastructure.Dtos.Payments;
using Microsoft.AspNetCore.Mvc;

namespace GestionHotel.Apis.Controllers;

[ApiController]
[ApiExplorerSettings(GroupName = "Bookings")]
[Route("bookings")]
public class BookingsController : GenericController<BookingsController>
{
    protected BookingsController(ILogger<GenericController<BookingsController>> logger) : base(logger)
    {
    }
    
    
    [HttpPost]
    [Route("add-booking/{userId:guid}/{roomId:guid}")]
    public ActionResult<Guid> AddBooking([FromRoute] Guid userId, [FromRoute] Guid roomId, [FromBody] PeriodeIn searchDates)
    {
        try
        {
            // var outDto = 
            return StatusCode(StatusCodes.Status200OK, "nothing now");
        }
        catch (Exception exception)
        {
            Logger.LogError("[RoomsController] GetAvailableRooms failed with the exception : {Exception}", exception.Message);
            throw;
        }
    }
    
    [HttpDelete]
    [Route("delete-booking/{userId:guid}")]
    public ActionResult<bool> DeleteBooking([FromRoute] Guid userId, [FromBody] RefundBookingIn refundInfos)
    {
        try
        {
            // var outDto = 
            return StatusCode(StatusCodes.Status200OK, "nothing now");
        }
        catch (Exception exception)
        {
            Logger.LogError("[RoomsController] GetAvailableRooms failed with the exception : {Exception}", exception.Message);
            throw;
        }
    }
    
    [HttpGet]
    [Route("handle-booking-arrival/{userId:guid}/{bookingId:guid}")]
    public ActionResult<BookingArrivalOut> HandleBookingArrival([FromRoute] Guid userId, [FromRoute] Guid bookingId)
    {
        try
        {
            // var outDto = 
            return StatusCode(StatusCodes.Status200OK, "nothing now");
        }
        catch (Exception exception)
        {
            Logger.LogError("[RoomsController] GetAvailableRooms failed with the exception : {Exception}", exception.Message);
            throw;
        }
    }
    
    [HttpGet]
    [Route("handle-booking-departure/{userId:guid}/{bookingId:guid}")]
    public ActionResult<BookingDepartureOut> HandleBookingDeparture([FromRoute] Guid userId, [FromRoute] Guid bookingId)
    {
        try
        {
            // var outDto = 
            return StatusCode(StatusCodes.Status200OK, "nothing now");
        }
        catch (Exception exception)
        {
            Logger.LogError("[RoomsController] GetAvailableRooms failed with the exception : {Exception}", exception.Message);
            throw;
        }
    }
}