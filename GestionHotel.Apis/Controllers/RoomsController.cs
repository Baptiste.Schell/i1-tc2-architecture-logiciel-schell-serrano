﻿using GestionHotel.Domain.Enums;
using GestionHotel.Domain.Interfaces.UseCases.Rooms;
using GestionHotel.Domain.Interfaces.UseCases.Users;
using GestionHotel.Infrastructure.Dtos.Others;
using GestionHotel.Infrastructure.Dtos.Rooms;
using Microsoft.AspNetCore.Mvc;

namespace GestionHotel.Apis.Controllers;

[ApiController]
[ApiExplorerSettings(GroupName = "Rooms")]
[Route("rooms")]
public class RoomsController : GenericController<RoomsController>
{
    private readonly IGetAvailableRoomsUseCase _getAvailableRoomsUseCase;
    private readonly IGetUserUseCase _getUserUseCase;
    
    protected RoomsController(ILogger<GenericController<RoomsController>> logger, IGetAvailableRoomsUseCase getAvailableRoomsUseCase, 
        IGetUserUseCase getUserUseCase) : base(logger)
    {
        _getAvailableRoomsUseCase = getAvailableRoomsUseCase;
        _getUserUseCase = getUserUseCase;
    }

    [HttpGet]
    [Route("get-available-rooms/{userId:guid}")]
    public ActionResult<IEnumerable<RoomOut>> GetAvailableRooms(Guid userId )
    {
        try
        {
            var user = _getUserUseCase.Handle(userId);
            
            var entities = _getAvailableRoomsUseCase.Handle(new DateTime(), new DateTime());
            
            // ---- Ici serait la partie mapper manquantes, alors nous mappons manuellement pour l'exemple ----- //
            
            var outDto = new List<RoomOut>();
            
            foreach (var room in entities)
            {
                var newRoom = new RoomOut()
                {
                    Capacity = room.Capacity,
                    Type = room.Type,
                    RoomId = room.Id,
                };

                if(user.Role == UserRole.Receptionist)
                {
                    newRoom.State = room.State;
                }
                
                outDto.Add(newRoom);
            }
            // ------------------------------------------------------------------------------------------------ //
            
            return StatusCode(StatusCodes.Status200OK, outDto);
        }
        catch (Exception exception)
        {
            Logger.LogError("[RoomsController] GetAvailableRooms failed with the exception : {Exception}", exception.Message);
            throw;
        }
    }
    
    [HttpGet]
    [Route("get-rooms-to-clean/{userId:guid}")]
    public ActionResult<List<RoomOut>> GetRoomsToClean([FromRoute] Guid userId, [FromBody] PeriodeIn searchDates)
    {
        try
        {
            // var outDto = 
            return StatusCode(StatusCodes.Status200OK, "nothing now");
        }
        catch (Exception exception)
        {
            Logger.LogError("[RoomsController] GetAvailableRooms failed with the exception : {Exception}", exception.Message);
            throw;
        }
    }
    
    [HttpPut]
    [Route("make-room-clean/{userId:guid}/{roomId:guid}")]
    public ActionResult<bool> MakeRoomClean([FromRoute] Guid userId, [FromRoute] Guid roomId)
    {
        try
        {
            // var outDto = 
            return StatusCode(StatusCodes.Status200OK, "nothing now");
        }
        catch (Exception exception)
        {
            Logger.LogError("[RoomsController] GetAvailableRooms failed with the exception : {Exception}", exception.Message);
            throw;
        }
    }
}