﻿using Microsoft.AspNetCore.Mvc;

namespace GestionHotel.Apis.Controllers;

public class GenericController<T> : Controller where T : class
{
    protected readonly ILogger<GenericController<T>> Logger;

    protected GenericController(ILogger<GenericController<T>> logger)
    {
        Logger = logger;
    }
}