using GestionHotel.Domain.Entities;

namespace GestionHotel.Domain.Interfaces.UseCases.Users;

public interface IGetUserUseCase
{
    public UserEntity Handle(Guid userId);
}