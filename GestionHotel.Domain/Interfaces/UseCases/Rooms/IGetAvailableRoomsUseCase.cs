using GestionHotel.Domain.Interfaces.Entities;

namespace GestionHotel.Domain.Interfaces.UseCases.Rooms;

public interface IGetAvailableRoomsUseCase
{
    public IEnumerable<IRoomEntity> Handle(DateTime startDate, DateTime endDate);
}