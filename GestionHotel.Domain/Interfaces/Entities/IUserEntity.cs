using GestionHotel.Domain.Enums;

namespace GestionHotel.Domain.Interfaces.Entities;

public interface IUserEntity
{
    public Guid Id { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public UserRole Role { get; set; }
}