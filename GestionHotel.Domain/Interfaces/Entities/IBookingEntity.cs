namespace GestionHotel.Domain.Interfaces.Entities;

public interface IBookingEntity
{
    public Guid Id { get; set; }
    public IRoomEntity ReservedRoom { get; set; }
    public IUserEntity Client { get; set; }
    public DateTime StartBooking { get; set; }
    public DateTime EndBooking { get; set; }
    public IEnumerable<IPaymentEntity> InvoiceList { get; set; }
}