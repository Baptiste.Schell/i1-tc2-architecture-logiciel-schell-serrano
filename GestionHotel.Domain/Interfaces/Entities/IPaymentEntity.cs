using GestionHotel.Domain.Enums;

namespace GestionHotel.Domain.Interfaces.Entities;

public interface IPaymentEntity
{
    public Guid Id { get; set; }
    public int Value { get; set; }
    public PaymentState State { get; set; }
}