using GestionHotel.Domain.Enums;

namespace GestionHotel.Domain.Interfaces.Entities;

public interface IRoomEntity
{
    public Guid Id { get; set; }
    public int Capacity { get; set; }
    public RoomType Type { get; set; } 
    public RoomState State { get; set; }
}
