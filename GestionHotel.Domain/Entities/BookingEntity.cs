using GestionHotel.Domain.Interfaces.Entities;

namespace GestionHotel.Domain.Entities;

public class BookingEntity : IBookingEntity
{
    public Guid Id { get; set; }
    public IRoomEntity ReservedRoom { get; set; } = new RoomEntity();
    public IUserEntity Client { get; set; } = new UserEntity();
    public DateTime StartBooking { get; set; }
    public DateTime EndBooking { get; set; }
    public IEnumerable<IPaymentEntity> InvoiceList { get; set; } = new List<IPaymentEntity>();
}