using GestionHotel.Domain.Enums;
using GestionHotel.Domain.Interfaces.Entities;

namespace GestionHotel.Domain.Entities;

public class UserEntity : IUserEntity
{
    public Guid Id { get; set; }
    public string FirstName { get; set; } = string.Empty;
    public string LastName { get; set; } = string.Empty;
    public UserRole Role { get; set; }
}