using GestionHotel.Domain.Enums;
using GestionHotel.Domain.Interfaces.Entities;

namespace GestionHotel.Domain.Entities;

public class RoomEntity : IRoomEntity
{
    public Guid Id { get; set; }
    public int Capacity { get; set; }
    public RoomType Type { get; set; }
    public RoomState State { get; set; }
}