using GestionHotel.Domain.Enums;
using GestionHotel.Domain.Interfaces.Entities;

namespace GestionHotel.Domain.Entities;

public class PaymentEntity : IPaymentEntity
{
    public Guid Id { get; set; }
    public int Value { get; set; }
    public PaymentState State { get; set; }
}