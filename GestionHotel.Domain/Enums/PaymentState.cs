﻿namespace GestionHotel.Domain.Enums;

public enum PaymentState
{
    Unpayed = 0,
    Payed = 1,
    InProcess = 2,
}