﻿namespace GestionHotel.Domain.Enums;

public enum RoomType
{
    Simple = 0,
    Double = 1,
    Suite = 2,
}