namespace GestionHotel.Domain.Enums;

public enum UserRole
{
    Client = 0,
    Receptionist = 1,
    HousekeepingStaff = 2
}