﻿namespace GestionHotel.Domain.Enums;

public enum RoomState
{
    New = 0,
    Redone = 1,
    ToBeRedone = 2,
    NothingToReport = 3,
    BigDamage = 4
}